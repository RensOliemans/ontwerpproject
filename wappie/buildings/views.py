import json
import urllib.request as req
from urllib.error import URLError

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render

from buildings.models import Building, Floor, Room
from items.models import Item

BASE_URL = 'http://iotdata01.utwente.nl/cgi-bin/utrooms.cgi'


def floor_items(request, building_name, floor_number):
    if request.is_ajax():
        floor = get_object_or_404(Floor, building__name=building_name, floor_number=floor_number)
        return JsonResponse({
            'items': [{'lon': i.location[0], 'lat': i.location[1], 'id': i.id, 'type': f"{i.type.image}"}
                      if i.location else {'id': i.id} for i in floor.items]
        })
    else:
        return HttpResponse()


def show_item(request):
    if request.is_ajax():
        item_id = request.GET.get('id', '')
        item = Item.objects.get(id=item_id)
        json_response = {'item': str(item)}

        return HttpResponse(json.dumps(json_response),
                            content_type='application/json')

    return render(request, 'index.html', {'user': ''})


def floor_geojson(request):
    building_name = request.GET.get('buildingName')
    floor_number = request.GET.get('floorNumber')

    url = f"{BASE_URL}?building={building_name}&floor={floor_number}"
    try:
        contents = req.urlopen(url).read()
    except URLError:
        return HttpResponse('Could not load building, check URL')
    return JsonResponse(json.loads(contents))


def overview(request):
    request.session.pop('building_name', None)
    request.session.pop('floor_number', None)
    first_floors = Floor.objects.filter(floor_number=1)
    first_floor_items = Item.objects.filter(floor__floor_number=1)
    homeless_items = Item.objects.items_without_location()
    return render(request, 'buildings/overview.html', {'first_floors_of_buildings': first_floors,
                                                       'items_on_first_floor': first_floor_items,
                                                       'homeless_items': homeless_items})


def building(request, building_name, floor_number=1):
    request.session['building_name'] = building_name
    request.session['floor_number'] = floor_number

    b = get_object_or_404(Building, name=building_name)
    f = get_object_or_404(Floor, building__name=building_name, floor_number=floor_number)
    homeless_items = Item.objects.items_without_location()
    return render(request, 'buildings/building.html',
                  {'building': b, 'floor': f, 'homeless_items': homeless_items})


def buildingLocation(request, building_name):
    building = get_object_or_404(Building, name=building_name)
    return JsonResponse({'lat': building.location[0], 'lon': building.location[1]})


def floor_rooms(request):
    floor_id = request.GET.get('floor')
    floor = get_object_or_404(Floor, id=floor_id)
    return render(request, 'items/room_dropdown_menu.html', {'rooms': floor.rooms})


def room_info(request):
    room_id = request.GET.get('room')
    room = get_object_or_404(Room, id=room_id)
    return JsonResponse({'lat': room.middle_of_room_location[0], 'lon': room.middle_of_room_location[1]})
