from django.test import TestCase
from django.urls import reverse

from buildings.models import Building, Floor
from wappie.test_utils import create_building, create_floor


class BuildingModelTests(TestCase):
    def test_floors_returns_all_floors(self):
        """
        Requesting the floors of a building gives all floors
        """
        b = Building(abbreviation='Zi', number=11, name='Zilverling')
        b.save()

        f1 = Floor.objects.create(building=b, floor_number=1)
        f2 = Floor.objects.create(building=b, floor_number=2)

        self.assertEqual(list(b.floors), list((f1, f2)))

    def test_correct_representation(self):
        b = Building.objects.create(abbreviation='Zi', number=11, name='Zilverling')
        self.assertEqual('Zilverling', str(b))
        self.assertEqual('<Building: Zilverling (Zi): 11>', repr(b))


class FloorModelTests(TestCase):
    def test_correct_representation(self):
        b = create_building()
        floor = Floor.objects.create(building=b, floor_number=11)
        self.assertEqual(f"{b.name}: 11", str(floor))
        self.assertEqual(f"<Floor: {b.name}.11>", repr(floor))


class BuildingFloorViewTests(TestCase):
    def test_no_floors(self):
        """
        Calling floors on a building without floors
        Expected logic: the building has no first floor, 404 is given
        """
        create_building()
        response = self.client.get(reverse('buildings:building',
                                           kwargs={'building_name': 'Zilverling'}))
        self.assertEqual(response.status_code, 404)

    def test_404_with_no_first_floor(self):
        create_floor(building=create_building(), floor_number=2)
        response = self.client.get(reverse('buildings:building',
                                           kwargs={'building_name': 'Zilverling'}))
        self.assertEqual(response.status_code, 404)

    def test_one_floor(self):
        """
        When one floor (!) is created, the response is the building with that floor
        """
        zilverling = create_building('ZI', 'Zilverling', 11)
        floor = create_floor(building=zilverling, floor_number=1)
        response = self.client.get(reverse('buildings:building',
                                           kwargs={'building_name': 'Zilverling'}))
        self.assertEqual(zilverling, response.context['building'])
        self.assertEqual(floor, response.context["floor"])

    def test_correct_floor(self):
        """
        When two floors (1 and 3) are created, the response has the building with those floors
        """
        zilverling = create_building('ZI', 'Zilverling', 11)
        floor_3 = create_floor(building=zilverling, floor_number=3)
        response = self.client.get(reverse('buildings:building_floor',
                                           kwargs={'building_name': 'Zilverling',
                                                   'floor_number': '3'}))
        self.assertEqual(floor_3, response.context['floor'])

    def test_non_existing_building(self):
        """
        When a non-existing building is requested, a 404 is raised
        """
        response = self.client.get(reverse('buildings:building',
                                           kwargs={'building_name': 'None'}))
        self.assertEqual(response.status_code, 404)
