from django import template

register = template.Library()


def caps(value):
    return ''.join([char.upper() for char in value])


register.filter('caps', caps)
