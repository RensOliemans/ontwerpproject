from django.urls import path

from buildings.views import overview, building, floor_items, show_item, buildingLocation, \
    floor_rooms, room_info, floor_geojson

app_name = 'buildings'

urlpatterns = [
    path('', overview, name='index'),
    path('room', room_info, name='room_info'),
    path('rooms/', floor_rooms, name='floor_rooms'),

    path('json/', floor_geojson, name='geojson'),

    path('<slug:building_name>/', building, name='building'),
    path('<slug:building_name>/location', buildingLocation, name='building_location'),
    path('<slug:building_name>/<int:floor_number>', building, name='building_floor'),
    path('<slug:building_name>/<int:floor_number>/items', floor_items, name='floor_items'),

    path('item_details/', show_item, name='item_details'),

]
