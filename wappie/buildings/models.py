from django.contrib.postgres.fields import ArrayField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from simple_history.models import HistoricalRecords


def get_default_location():
    return [52.2400, 6.8530]


class Building(models.Model):
    abbreviation = models.CharField(
        max_length=2,
        unique=True,
        verbose_name="abbreviation",
    )
    name = models.CharField(
        max_length=30,
        unique=True,
        verbose_name="building name"
    )
    number = models.PositiveIntegerField(
        unique=True,
        verbose_name="building number"
    )
    location = ArrayField(
        base_field=models.FloatField(),
        size=2,
        # Can be blank or null: if a WAP is created but has no location yet
        default=get_default_location,
        verbose_name="location",
        help_text="this is equal to the postgresql 'point' field: tuple of floats"
    )
    history = HistoricalRecords()

    @property
    def floors(self):
        return self.floor.all().order_by('floor_number')

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<Building: {self.name} ({self.abbreviation}): {self.number}>"

    class Meta:
        ordering = ['name']


class Floor(models.Model):
    building = models.ForeignKey(
        to=Building,
        on_delete=models.PROTECT,
        related_name="floor",  # This allows you to do `building.floor` and get the related floor(s)
        verbose_name="building of floor"
    )
    floor_number = models.IntegerField(
        validators=[MaxValueValidator(200), MinValueValidator(-100)],
        verbose_name="floor number"
    )
    history = HistoricalRecords()

    def __str__(self):
        return f"{self.building.name}: {self.floor_number}"

    def __repr__(self):
        return f"<Floor: {self.building.name}.{self.floor_number}>"

    @property
    def items(self):
        return self.item.all()

    @property
    def waps(self):
        return [wap for wap in self.item.all() if hasattr(wap, 'wap')]

    @property
    def rooms(self):
        return self.room.all()

    class Meta:
        unique_together = (('building', 'floor_number'),)
        ordering = ['building__name', 'floor_number']


class Room(models.Model):
    floor = models.ForeignKey(
        to=Floor,
        on_delete=models.PROTECT,
        related_name="room",
        verbose_name="floor of building"
    )
    room_code = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        verbose_name="room code"
    )
    middle_of_room_location = ArrayField(
        base_field=models.FloatField(),
        size=2,
        null=True,
        blank=True,
        verbose_name="middle_room",
        help_text="the middle of the room."
    )
    history = HistoricalRecords()

    def __str__(self):
        return self.room_code

    def __repr__(self):
        return f"<Room: {repr(self.floor)} - {self.room_code}, " \
               f"location: {self.middle_of_room_location}>"

    class Meta:
        unique_together = (('floor', 'room_code'),)
        ordering = ['floor__building__name', 'floor__floor_number', 'room_code']
