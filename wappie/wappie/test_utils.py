from buildings.models import Building, Floor, Room
from items.models import ItemType


def create_building(abbreviation='ZI', name='Zilverling', number=11):
    """
    Creates and returns a building, defaults to Zilverling (ZI, number 11)
    :param abbreviation: Abbreviation of building
    :param name: Name of building
    :param number: Number of building
    :return: Zilverling building object
    """
    return Building.objects.create(name=name, number=number, abbreviation=abbreviation)


def create_floor(building, floor_number=1):
    """
    Creates and returns a floor, defaults to floor_number 1
    :param building: building the floor is located on
    :param floor_number: number of the floor, defaults to 1
    :return: Floor building object
    """
    building = building or create_building()
    return Floor.objects.create(building=building, floor_number=floor_number)


def create_room(floor, room_code='1234'):
    """
    Creates and returns a room, defaults to room_code 1234
    :param floor: floor the room is on
    :param room_code: code of the room
    :return: Room object
    """
    floor = floor or create_floor(create_building())
    return Room.objects.create(floor=floor, room_code=room_code)


def create_item_type(name='WAP'):
    """
    Creates and returns an type
    :param name: name of the item type
    :return: ItemType created
    """
    return ItemType.objects.create(type=name)
