from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from wappie.views import index

urlpatterns = [
    # Home page
    path('', index, name='home'),

    # Apps
    path('buildings/', include('buildings.urls'), name='buildings'),
    path('items/', include('items.urls'), name='items'),

    # Admin
    path('admin/', admin.site.urls)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
