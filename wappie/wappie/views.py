from django.shortcuts import render

from buildings.models import Building
from items.models import Item, ItemType


def index(request):
    homeless_items = Item.objects.items_without_location()
    item_types = ItemType.objects.all()
    nr_of_items = Item.objects.all().count()
    nr_of_buildings = Building.objects.all().count()
    nr_of_floors = 0
    for building in Building.objects.all():
        nr_of_floors += building.floors.count()
    nr_of_items_per_type = []
    for type in item_types:
        nr_of_items_per_type.append(Item.objects.filter(type=type).count())
    return render(request, template_name='index.html', context={'homeless_items': homeless_items, 'item_types': item_types,
                                                                'counted_items_per_type': nr_of_items_per_type, 'total_nr_of_items': nr_of_items,
                                                                'nr_of_buildings': nr_of_buildings, 'nr_of_floors': nr_of_floors})
