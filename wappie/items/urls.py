from django.urls import path

from items.views import ItemUpdate, delete, replace, ItemCreate, history, items, export
from items.type_views import ItemTypeCreate, ItemTypeUpdate, item_type_delete

app_name = 'items'

urlpatterns = [
    path('', items, name='index'),

    # Items
    path('edit/<int:pk>', ItemUpdate.as_view(), name='edit'),
    path('delete/<int:pk>', delete, name='delete'),
    path('replace/<int:pk>', replace, name='replace'),
    path('add/', ItemCreate.as_view(), name='add'),
    path('history/<int:pk>', history, name='history'),
    path('export', export, name='export'),

    # Item Types
    path('types/add/', ItemTypeCreate.as_view(), name='add_type'),
    path('types/update/<int:pk>', ItemTypeUpdate.as_view(), name='update_type'),
    path('types/delete/<int:pk>', item_type_delete, name='delete_type'),
]
