# Generated by Django 2.1.7 on 2019-04-10 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0003_delete_tmp'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='itemtype',
            options={'ordering': ['type']},
        ),
        migrations.AlterField(
            model_name='itemtype',
            name='image',
            field=models.FileField(default='items/wifi-icon.png', upload_to='media/'),
        ),
    ]
