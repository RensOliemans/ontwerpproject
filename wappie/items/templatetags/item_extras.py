from django import template

register = template.Library()


def is_wap(value):
    return hasattr(value, 'wap')


register.filter('is_wap', is_wap)
