from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import UpdateView

from items.forms.create_item_type import ItemTypeCreateForm, ItemTypeUpdateForm
from items.models import ItemType


class ItemTypeCreate(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'items/itemtype_add_form.html',
                      {'form': ItemTypeCreateForm()})

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel'):
            return redirect(self.get_success_url())
        f = ItemTypeCreateForm(request.POST, request.FILES)
        if f.is_valid():
            f.save()
            return redirect(self.get_success_url())
        return render(request, 'items/itemtype_add_form.html', {'form': f})

    def get_success_url(self):
        return reverse('items:index')


class ItemTypeUpdate(UpdateView):
    model = ItemType
    form_class = ItemTypeUpdateForm
    template_name_suffix = '_update_form'

    def post(self, request, *args, **kwargs):
        if request.POST.get('delete'):
            ItemType.objects.get(id=kwargs['pk']).delete()
            return redirect(self.get_success_url())
        elif request.POST.get('cancel'):
            return redirect(self.get_success_url())
        f = ItemTypeUpdateForm(request.POST, request.FILES, instance=ItemType.objects.get(id=kwargs['pk']))
        if f.is_valid():
            f.save()
            return redirect(self.get_success_url())
        return render(request, 'items/itemtype_update_form.html', {'form': f})

    def get_success_url(self):
        return reverse('items:index')


def item_type_delete(request, pk):
    item_type = ItemType.objects.get(id=pk)
    item_type.delete()
    return redirect('/items/')
