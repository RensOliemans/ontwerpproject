from django import forms

from buildings.models import Room
from items.models import Item


class ItemCreateForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['type', 'floor', 'room', 'location', 'location_description', 'start_date', 'mac']

    def __init__(self, *args, **kwargs):
        super(ItemCreateForm, self).__init__(*args, **kwargs)
        self.fields['room'].queryset = Room.objects.none()

        if 'floor' in self.data:
            try:
                floor_id = int(self.data.get('floor'))
                self.fields['room'].queryset = Room.objects.filter(floor_id=floor_id).order_by('room_code')
            except (ValueError, TypeError):
                pass


class ItemUpdateForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['type', 'floor', 'room', 'location', 'location_description', 'start_date', 'mac']

    def __init__(self, *args, **kwargs):
        super(ItemUpdateForm, self).__init__(*args, **kwargs)
        if 'room' not in self.data:
            self.fields['room'].queryset = Room.objects.none()

        if 'floor' in self.data:
            try:
                floor_id = int(self.data.get('floor'))
                self.fields['room'].queryset = Room.objects.filter(floor_id=floor_id).order_by('room_code')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            if self.instance.floor:
                self.fields['room'].queryset = self.instance.floor.rooms.order_by('room_code')
