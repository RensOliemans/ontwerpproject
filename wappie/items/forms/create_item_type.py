from django import forms

from items.models import ItemType


class ItemTypeCreateForm(forms.ModelForm):
    class Meta:
        model = ItemType
        fields = ['type', 'image']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ItemTypeUpdateForm(forms.ModelForm):
    class Meta:
        model = ItemType
        fields = ['type', 'image']

    def __init___(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
