from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from wappie.test_utils import create_building, create_floor, create_room, create_item_type
from items.models import Item


class ItemModelTests(TestCase):
    def test_location_correct_size(self):
        """
        Location has to have a size of 2
        """
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        i = Item(floor=f, location=(1.0, 4.0),
                 location_description="Right", start_date=timezone.now(), type=t, mac='1')
        try:
            i.save()
        except ValidationError:
            self.fail("Should not have failed")

    def test_location_negative_float(self):
        """
        Location can be negative and floats
        """
        b = create_building()
        f = create_floor(b)
        i = Item(floor=f, location=(52.34384883, -4.25),
                 location_description="Right", start_date=timezone.now(),
                 type=create_item_type(), mac='1')
        try:
            i.save()
        except ValidationError:
            self.fail("Should not have failed")

    def test_location_too_large(self):
        """
        Location can't have more than 2 elements
        """
        b = create_building()
        f = create_floor(b)
        i = Item(floor=f, location=(52.34384883, -4.25),
                 location_description="Right", start_date=timezone.now(),
                 type=create_item_type(), mac='1')
        i.save()
        self.assertEqual(i.location, (52.34384883, -4.25))

    def test_location_cannot_have_one_element(self):
        f = create_floor(create_building())
        i = Item(floor=f, location=2, start_date=timezone.now(), mac='1')
        self.assertRaises(ValidationError, i.save)

    def test_location_should_be_correct_geo_format(self):
        """
        See https://epsg.io/5819
        """
        f = create_floor(create_building())
        t = create_item_type()
        now = timezone.now()
        correct_item_1 = Item(floor=f, location=(-90, -180), start_date=now, type=t, mac='1')
        correct_item_2 = Item(floor=f, location=(-90, 180), start_date=now, type=t, mac='2')
        correct_item_3 = Item(floor=f, location=(0, 0), start_date=now, type=t, mac='3')
        item_too_large_lat_minus = Item(floor=f, location=(-91, 0), start_date=now, type=t, mac='4')
        item_too_large_lat = Item(floor=f, location=(90.5, 89.3), start_date=now, type=t, mac='5')

        try:
            correct_item_1.save()
            correct_item_2.save()
            correct_item_3.save()
        except ValidationError:
            self.fail("These items have correct geo locations")

        self.assertRaises(ValidationError, item_too_large_lat_minus.save)
        self.assertRaises(ValidationError, item_too_large_lat.save)

    def test_longitude_should_wrap_around(self):
        f = create_floor(create_building())
        item_very_large_lon = Item(floor=f, location=(15, 190), start_date=timezone.now(),
                                   type=create_item_type(), mac='1')
        item_very_large_lon.save()
        self.assertEqual(item_very_large_lon.location, (15, -170))

    def test_homeless_items_shown_correctly(self):
        """
        Homeless items queryset works as intended
        """
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        Item.objects.create(floor=f, location=(52.2, 6.8),
                            start_date=timezone.now(), type=t, mac='1')
        item_without_location = Item.objects.create(start_date=timezone.now(), type=t, mac='2')
        self.assertQuerysetEqual(Item.objects.items_without_location(),
                                 [repr(item_without_location)])

    def test_cannot_add_item_with_location_but_no_floor(self):
        i = Item(location=(52.6, 6.8), start_date=timezone.now())
        self.assertRaises(ValidationError, i.save)

    def test_item_has_room(self):
        b = create_building()
        f = create_floor(b)
        r = create_room(f)
        t = create_item_type()
        i = Item(location=(52.6, 6.5), start_date=timezone.now(),
                 floor=f, room=r, type=t, mac='1')
        try:
            i.save()
        except ValidationError:
            self.fail("Save should have succeeded")

    def test_room_should_be_in_floor(self):
        b = create_building()
        first_floor = create_floor(b, floor_number=1)
        second_floor = create_floor(b, floor_number=2)
        r = create_room(first_floor)
        i = Item(floor=second_floor, room=r, start_date=timezone.now(), mac='1')
        self.assertRaises(ValidationError, i.save)

    def test_if_room_is_given_floor_must_be_give(self):
        b = create_building()
        f = create_floor(b)
        r = create_room(f)
        i = Item(room=r, start_date=timezone.now(), mac='1')
        self.assertRaises(ValidationError, i.save)


class WAPModelTests(TestCase):
    def test_validate_mac_address_empty(self):
        """
        Empty mac address raises an exception
        """
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        w = Item(floor=f, type=t, location=(1.0, 4.0),
                 location_description="Left", start_date=timezone.now())
        self.assertRaises(ValidationError, w.save)

    def test_validate_mac_address_invalid(self):
        """
        Invalid mac address raises an exception
        """
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        w = Item(floor=f, type=t, location=(1.0, 4.0), location_description="Left",
                 start_date=timezone.now(), mac='ag')
        self.assertRaises(ValidationError, w.save)

    def test_validate_mac_address_valid(self):
        """
        Valid mac addresses don't raise exceptions
        """
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        w1 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='01', type=t)
        w2 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac=3, type=t)
        w3 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='0123456789aa', type=t)
        w4 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='01-23-45-67-89-ab', type=t)
        w5 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='01:23:45:67:89:ac', type=t)
        try:
            w1.save()
            w2.save()
            w3.save()
            w4.save()
            w5.save()
        except ValidationError:
            self.fail("w.save() raised an exception when it shouldn't have")

    def test_validate_mac_address_not_unique_same_token(self):
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        w1 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='0123456789aa', type=t)
        w2 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='01:23:45:67:89:aa', type=t)
        w1.save()
        self.assertRaises(ValidationError, w2.save)

    def test_validate_mac_address_not_unique_number_to_hex(self):
        b = create_building()
        f = create_floor(b)
        t = create_item_type()
        w1 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='111', type=t)
        w2 = Item(floor=f, location=(1.0, 4.0), location_description="Left",
                  start_date=timezone.now(), mac='6F', type=t)
        w1.save()
        self.assertRaises(ValidationError, w2.save)


class ItemViewTests(TestCase):
    def test_floor_with_no_items(self):
        """
        You have a floor with no items. Calling the floor view on it results in a view
        with no items, but no raised exception
        """
        b = create_building()
        f = create_floor(b)
        response = self.client.get(reverse('buildings:floor_items',
                                           kwargs={'building_name': b.name,
                                                   'floor_number': f.floor_number}))
        self.assertEqual(response.status_code, 200)
