from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.postgres.fields import ArrayField
from geopy import Point

from netaddr import EUI, AddrFormatError
from simple_history.models import HistoricalRecords

from buildings.models import Floor, Room


def mac_validator(mac):
    """
    Method which validates the mac in question
    :param mac: address to validate
    :raises ValidationError: when address is invalid
    """
    try:
        return EUI(mac)
    except (ValueError, AddrFormatError, TypeError):
        raise ValidationError("%(mac)s is not a valid MAC address.",
                              code='invalid',
                              params={'mac': mac})


class ItemQuerySet(models.QuerySet):
    def items_without_location(self):
        return self.filter(floor__isnull=True)


class ItemType(models.Model):
    type = models.CharField(
        max_length=40,
        unique=True,
        verbose_name="Type of the item.",
        help_text="What type the item is, for example: WAP, or Dispenser"
    )
    image = models.FileField(
        upload_to='',
        default='items/wifi-icon.png'
    )

    def __repr__(self):
        return f"<ItemType: type: {self.type}>"

    def __str__(self):
        return self.type

    class Meta:
        ordering = ['type']


def get_generic_type():
    return ItemType.objects.get_or_create(type='Generic')[0].id


class Item(models.Model):
    """
    Generic Item model.
    """
    floor = models.ForeignKey(
        to=Floor,
        on_delete=models.PROTECT,
        related_name="item",
        blank=True,
        null=True,
        verbose_name="The floor of the item."
    )
    room = models.ForeignKey(
        to=Room,
        on_delete=models.PROTECT,
        related_name="item",
        blank=True,
        null=True,
        verbose_name="The room of the item."
    )
    type = models.ForeignKey(
        to=ItemType,
        on_delete=models.SET_DEFAULT,
        default=get_generic_type,
        null=False,
        verbose_name="Type of the item."
    )
    location = ArrayField(
        base_field=models.FloatField(),
        size=2,
        # Can be blank or null: if a WAP is created but has no location yet
        blank=True,
        null=True,
        verbose_name="location",
        help_text="Enter two numbers (geolocation), seperated with a comma."
    )
    location_description = models.TextField(
        blank=True,
        null=True,
        verbose_name="location description",
        help_text="Textual description of the location."
    )
    start_date = models.DateTimeField(
        verbose_name="start date",
        help_text="The datetime the item was put here"
    )
    end_date = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="end date",
        help_text="The datetime of the end of the item."
    )
    remarks = models.TextField(
        null=True,
        blank=True,
        verbose_name="remarks",
        help_text="Any possible remarks."
    )
    mac = models.CharField(
        max_length=17,
        unique=True,
        null=False,
        verbose_name="mac address",
        help_text="mac address of the wap"
    )
    history = HistoricalRecords()

    objects = ItemQuerySet.as_manager()

    def save(self, *args, **kwargs):
        self.clean()
        self.full_clean()  # We need a full_clean so it will validate the location
        super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        self.mac = mac_validator(self.mac)
        self.check_correct_location()
        self.check_no_location_without_floor()
        self.check_room_is_in_floor()
        self.check_no_room_without_floor()

    def check_correct_location(self):
        try:
            if self.location:
                lat = self.location[0]
                lon = self.location[1]
                point = Point(latitude=lat, longitude=lon)
                self.location = (point.latitude, point.longitude)
        except (TypeError, KeyError, ValueError, IndexError):
            raise ValidationError(f"Location was of incorrect format.")

    def check_no_location_without_floor(self):
        if self.floor is None and self.location:
            raise ValidationError(f"Location is given yet floor is not given.\n"
                                  f"In order to choose a location, a floor must be given.")

    def check_room_is_in_floor(self):
        if self.room is not None and self.room.floor != self.floor:
            raise ValidationError(f"The given room ({self.room}) should be in the the given "
                                  f"floor ({self.floor}).")

    def check_no_room_without_floor(self):
        if self.room and not self.floor:
            raise ValidationError(f"If a room is given (now {self.room}), the floor should be given"
                                  f" as well.")

    def __str__(self):
        return f"{self.type}, at {self.location} on {self.floor}"

    def __repr__(self):
        location_text = "no floor"
        if self.floor:
            location_text = f"at {self.location} on {self.floor.building.abbreviation}." \
                            f"{self.floor.floor_number}"

        return f"<Item: {self.type} at " \
               f"{location_text}, " \
               f"from {self.start_date} to {self.end_date}>"
