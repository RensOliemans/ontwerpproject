from django.contrib import admin

from items.exporter import ItemAdmin
from items.models import Item

admin.site.register(Item, ItemAdmin)
