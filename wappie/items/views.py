from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.utils import timezone
from django.views import View

from buildings.models import Floor
from items.exceptions import SessionNotSetException
from items.exporter import ItemResource
from items.forms.item_forms import ItemCreateForm, ItemUpdateForm
from items.models import Item, ItemType

SUPPORTED_FORMAT = ['csv', 'json', 'xls', 'yaml', 'html', 'ods']


def items(request):
    items = Item.objects.all()
    item_types = ItemType.objects.all()
    return render(request, 'items/items.html', {'items': items, 'item_types': item_types,
                                                'formats': SUPPORTED_FORMAT})


def showItem(request, item_name):
    item = get_object_or_404(Item, name=item_name)
    return HttpResponse(",".join([str(item)]))


def replace(request, pk):
    item = Item.objects.get(id=pk)
    item.end_date = timezone.now()
    replace_item = Item()
    replace_item.start_date = timezone.now()
    replace_item.location = item.location
    replace_item.type = item.type
    replace_item.location_description = item.location_description
    replace_item.floor = item.floor
    replace_item.room = item.room
    replace_item.mac = item.mac
    item.delete()
    replace_item.save()
    return redirect('/items/edit/' + str(replace_item.id))


def delete(request, pk):
    item = Item.objects.get(id=pk)
    item.end_date = timezone.now()
    item.save()
    item.delete()
    return redirect('/buildings/')


def get_information_from_get_request(request):
    lon = request.get('lon')
    lat = request.get('lat')
    building_name = request.get('buildingName')
    floor_number = request.get('floorNumber')
    floor = None
    if building_name and floor_number:
        floor = Floor.objects.get(building__name=building_name, floor_number=floor_number)
    return lon, lat, floor


def history(request, pk):
    item = Item.objects.get(id=pk)
    item_history = item.history.all()
    history_list = [_build_neat_history_overview(entry) for entry in item_history]
    return JsonResponse(history_list, safe=False)


def _build_neat_history_overview(history_entry):
    try:
        lon, lat = history_entry.location
    except ValueError:
        lon, lat = '', ''
    return {'type': str(history_entry.type.image),
            'floor': str(history_entry.floor),
            'room': str(history_entry.room),
            'lat': lat,
            'lon': lon,
            'mac': history_entry.mac,
            'description': history_entry.location_description,
            'date of change': history_entry.history_date.strftime('%Y-%m-%d'),
            'time of change': history_entry.history_date.strftime('%H:%M:%S %Z')}


class ItemCreate(View):
    def get(self, request, *args, **kwargs):
        lon, lat, floor = get_information_from_get_request(request.GET)

        if floor:
            self.request.session['building_name'] = floor.building.name
            self.request.session['floor_number'] = floor.floor_number
        else:
            self.request.session.pop('building_name', None)
            self.request.session.pop('floor_number', None)

        form = build_create_form(lat, lon, floor)
        return render(request, 'items/item_add_form.html',
                      {'form': form})

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel'):
            return redirect(self.get_success_url())
        f = ItemCreateForm(request.POST)
        if f.is_valid():
            f.save()
            return redirect(self.get_success_url())
        return render(request, 'items/item_add_form.html', {'form': f})

    def get_success_url(self):
        try:
            kwargs = get_kwargs_of_building(self.request.session)
            return reverse('buildings:building_floor', kwargs=kwargs)
        except SessionNotSetException:
            return reverse('buildings:index')


def build_create_form(lat, lon, floor):

    form = ItemCreateForm(initial={
        'location': f"{lat},{lon}" if lat and lon else '',
        'start_date': timezone.now(),
        'floor': floor})
    return form


class ItemUpdate(View):
    model = Item
    form_class = ItemUpdateForm
    template_name_suffix = '_update_form'

    def get(self, request, *args, **kwargs):
        item_id = kwargs['pk']
        item = Item.objects.get(id=item_id)

        lon, lat, floor = get_information_from_get_request(request.GET)
        form = build_update_form(floor or item.floor, item, lat, lon)
        return render(request, 'items/item_update_form.html',
                      {'form': form})

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel'):
            return redirect(self.get_success_url())
        elif request.POST.get('delete'):
            Item.objects.get(id=kwargs['pk']).delete()
            return redirect(self.get_success_url())
        if request.is_ajax():
            item, location, date = get_information_from_post_request(request.POST)
            if location and not item.floor:
                return
            item.location = location
            item._history_date = date
            item.save()
            return HttpResponse()
        form = ItemUpdateForm(request.POST, instance=Item.objects.get(id=kwargs['pk']))
        if form.is_valid():
            form.save()
            return redirect(self.get_success_url())
        return render(request, 'items/item_update_form.html', {'form': form})

    def get_success_url(self):
        try:
            kwargs = get_kwargs_of_building(self.request.session)
            return reverse('buildings:building_floor', kwargs=kwargs)
        except SessionNotSetException:
            return reverse('buildings:index')


def build_update_form(floor, item, lat=None, lon=None):
    location = f"{lat},{lon}" if lat and lon else None
    form = ItemUpdateForm(initial={'floor': floor, 'location': location or item.location,
                                   'room': item.room},
                          instance=item)
    return form


def get_information_from_post_request(request):
    lat = request.get("lat", "")
    lon = request.get("lon", "")
    date = request.get("date", "")
    item_id = request.get("id", "")
    item = Item.objects.get(id=item_id)
    location = [lat, lon]
    return item, location, date


def get_kwargs_of_building(session):
    fields = ['building_name', 'floor_number']
    if all(key in session for key in fields) and all(session[key] for key in fields):
        return {key: session[key] for key in fields}
    raise SessionNotSetException("Not all fields were set in the users's session.")


def export(request):
    export_format = request.GET.get('format')
    output = ItemResource().export(Item.objects.all())
    try:
        response = HttpResponse(getattr(output, export_format),
                                content_type='application/force-download')
        response['Content-Disposition'] = f'attachment; ' \
                                          f'filename="item_dump_{timezone.now().isoformat()}.{export_format}"'
        return response
    except AttributeError:
        return redirect('items:index')
