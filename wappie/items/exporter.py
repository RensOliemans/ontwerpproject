from import_export import resources
from import_export.admin import ImportExportModelAdmin

from items.models import Item


class ItemResource(resources.ModelResource):
    class Meta:
        model = Item


class ItemAdmin(ImportExportModelAdmin):
    resource_class = ItemResource
