function showHistory(itemId) {
    $.ajax({
        url: "/items/history/" + itemId,
        success: function (data) {
            let content_string = buildHistoryContent(data);
            $("#history-list").html(content_string);
            $("#itemIdParagraph").html("ID of item: " + itemId);

            document.getElementById("historyMenu").style.display = "block";
        }
    });
}

function buildHistoryContent(data) {
    console.log(data);
    let total = "";
    data.forEach(function element(itemHistory) {
        total += (buildString(itemHistory));
    });
    return total;
}

function buildString(itemHistory) {
    let itemHistoryElements = [];
    let imageSource = '';
    Object.entries(itemHistory).forEach(function appendEntry([key, value]) {
        if (key === 'type') {
            imageSource = 'media/' + value;
            return;
        }

        if (value)
            itemHistoryElements.push(key.capitalizeFirstLetter() + ': ' + value);
        else
            itemHistoryElements.push('No ' + key);
    });

    let finalString = itemHistoryElements.join('.<br/>');
    if (imageSource)
        finalString = getImageHtml(imageSource) + finalString;

    return wrapEntryInListElement(finalString);
}

function getImageHtml(imageSource) {
    return "<img src=\"/" + imageSource + "\" height=\"35px\" alt=\"Icon\" class=\"mr-3\">";
}

function wrapEntryInListElement(entry) {
    return "<li class=\"media list-group-item\">" + entry + "</li>"
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
};