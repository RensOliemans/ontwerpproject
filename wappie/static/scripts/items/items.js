function fillRoomsIfFloorExists() {
    let url = $("#itemForm").attr("item-room-url");
    let floor = $("#id_floor");
    if (floor.val())
        getAndSetRooms(url, floor.val());
}


function getRoomsOfFloor() {
    $("#id_floor").change(function () {
        var url = $("#itemForm").attr("item-room-url");
        var floorId = $(this).val();

        getAndSetRooms(url, floorId);
    });
}

function getAndSetRooms(url, floorId) {
    $.ajax({
        url: url,
        data: {
            'floor': floorId
        },
        success: function setRoomSelect(data) {
            $("#id_room").html(data);
        }
    });
}

function fillLocationWhenFloorIsSelected() {
    $("#id_room").change(function () {
        let url = $("#itemForm").attr("room-location-url");
        let roomId = $(this).val();
        let location = document.getElementById("id_location").value;

        if (!location) {
            $.ajax({
                url: url,
                data: {
                    'room': roomId
                },
                success: function setLocation(data) {
                    let formatted_data = '' + data['lat'] + ', ' + data['lon'];
                    document.getElementById("id_location").value = formatted_data;
                }
            });
        }
    });
}