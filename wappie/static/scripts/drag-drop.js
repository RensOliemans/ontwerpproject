window.buildingName = '';
window.floorNumber = '';

function setBuildingName(buildingName) {
        window.buildingName = buildingName;
}

function setFloorNumber(floorNumber) {
    window.floorNumber = floorNumber;
}

function dragStart(event) {
    setId(event);
}

function handleDrop(event) {
    event.preventDefault();
    let id = getId(event);
    let coordinates = getCurrentMouseLocation(event);
    goToEditPage(id, coordinates);
}

function setId(event) {
    event.dataTransfer.setData("id", event.target.id);
}

function getId(event) {
    return event.dataTransfer.getData("id");
}

function allowDrop(event) {
    event.preventDefault();
}


function getCurrentMouseLocation(event) {
    let coordinates = olMap.getCoordinateFromPixel(
        [event.x, event.y]
    );
    return ol.proj.transform(coordinates, 'EPSG:3857', 'EPSG:4326');
}

function goToEditPage(id, lonLat) {
    let url = '/items/edit/' + id + '?lon=' + lonLat[0] + '&lat=' + lonLat[1];
    if (window.buildingName) url += '&buildingName=' + window.buildingName;
    if (window.floorNumber) url += '&floorNumber=' + window.floorNumber;

    window.location = url;
}