const tileTitle = 'tileLayer';
const buildingTitle = 'buildingLayer';
const itemTitle = 'itemLayer';

const tileZIndex = 0;
const buildingZIndex = 1;
const itemZIndex = 2;
var UTView = [6.8530,52.2400];

var olMap = new ol.Map({
    target: 'map',
    layers: [
        new ol.layer.Tile({
            title: tileTitle,
            source: new ol.source.OSM(),
            zIndex: tileZIndex
        })
    ],
    view: new ol.View({
        center: ol.proj.fromLonLat(UTView),
        zoom: 16,
        projection: 'EPSG:3857'
    })
});

function setupOverviewPage() {
    addTranslateInteraction();
    linkToBuildingPage();
    buildContextMenu();
}

function setupBuildingPage(buildingName, floorNumber) {
    addTranslateInteraction();
    buildContextMenu(buildingName, floorNumber);
}


function buildContextMenu(buildingName, floorNumber) {
    let ownDefaultMenuItems = getDefaultMenuItems();
    ownDefaultMenuItems[0].data = {'buildingName': buildingName, 'floorNumber': floorNumber};
    let contextMenu = getDefaultContextMenu(ownDefaultMenuItems);
    olMap.addControl(contextMenu);

    var shouldRebuildDefaultMenu = false;
    contextMenu.on('open', function mapWasClicked(evt) {
        let clickedItemFeature = olMap.forEachFeatureAtPixel(
            evt.pixel,
            function getClickedFeature(feature, layer) {
                return feature;
            },
            {
                layerFilter:
                    function layerClickedIsItemLayer(layer) {
                        return layer.get('title') === itemTitle;
                    }
            }
        );

        if (clickedItemFeature != null) {
            let itemId = clickedItemFeature.values_.id;
            let itemsToAdd = getItemContextMenus(itemId, buildingName);
            overrideExistingContextMenu(contextMenu, [itemsToAdd]);
            shouldRebuildDefaultMenu = true;
        } else if (shouldRebuildDefaultMenu) {
            overrideExistingContextMenu(contextMenu, [ownDefaultMenuItems]);
            shouldRebuildDefaultMenu = false;
        }
    });
}

function getDefaultMenuItems() {
    return [{
        text: 'Add',
        callback: goToAddPage
    }];
}

var goToAddPage = function (location) {
    let coordinates = ol.proj.transform(location.coordinate, 'EPSG:3857', 'EPSG:4326');
    let lon = coordinates[0];
    let lat = coordinates[1];
    console.log(location.data);
    window.location = buildUrl(lon, lat, location.data);
};

function buildUrl(lon, lat, floorData) {
    let url = '/items/add' + '?lon=' + lon + '&lat=' + lat;
    if (floorData.buildingName !== undefined) url += '&buildingName=' + floorData.buildingName;
    if (floorData.floorNumber !== undefined) url += '&floorNumber=' + floorData.floorNumber;
    return url;
}

function getDefaultContextMenu(menuItems) {
    return new ContextMenu({
        width: 170,
        defaultItems: false,
        items: menuItems
    });
}


function overrideExistingContextMenu(existingContextMenu, items) {
    resetContextMenu(existingContextMenu);
    extendItemsToContextMenu(existingContextMenu, items);
}


function resetContextMenu(contextMenu) {
    contextMenu.clear();
}


function extendItemsToContextMenu(contextMenu, items) {
    items.forEach(function add(item) {
        contextMenu.extend(item);
    });
}

function getItemContextMenus(itemId, buildingName) {
    return [
        {
            text: 'Edit',
            callback: function editCall() {
                    let url = "/items/edit/" + itemId;
                    if (buildingName) url += "?buildingName=" + buildingName;
                    document.location.href = url;
                }
        },
        {
            text: 'Replace',
            callback: function replaceCall() {
                    document.location.href = "/items/replace/" + itemId
                }
        },
        {
            text: 'Delete',
            callback: function deleteCall() {
                    document.location.href = "/items/delete/" + itemId
                }
        },
        {
            text: 'Show history',
            callback: function history() {
                showHistory(itemId);
            },
        }
    ];
}

window.addEventListener( "pageshow", function ( event ) {
  var historyTraversal = event.persisted ||
                         ( typeof window.performance != "undefined" &&
                              window.performance.navigation.type === 2 );
  if ( historyTraversal ) {
    // Handle page restore.
    window.location.reload();
  }
});

function load_floor_on_map(url) {
    fetch (url)
        .then(function(response) {
            return response.json();
        })
        .then(function(geoJSON) {
            console.log(geoJSON);
            var layer = new ol.layer.Vector({
                title: buildingTitle,
                source: new ol.source.Vector({
                    features: (new ol.format.GeoJSON()).readFeatures(geoJSON, {
                        dataProjection: 'EPSG:4326',
                        featureProjection: 'EPSG:3857'
                    })
                }),
                style: function (feature) {
                    return new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(0, 148, 179, 0.5)',
                            width: 2
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(0, 148, 179, 0.1)'
                        }),
                        text: new ol.style.Text({
                            font: '12px Calibri,sans-serif',
                            text: feature.get('name')
                        })
                    })
                }
            });
            layer.setZIndex(buildingZIndex);
            olMap.addLayer(layer);
        });
}

let idToIconMapping = {};

function load_items_on_map(lon, lat, item_id, icon) {
    let coordinates = buildCoordinates(lon, lat);
    let point = new ol.geom.Point(coordinates);

    idToIconMapping[item_id] = '/media' + icon;
    let features = getFeatures(point, item_id);

    let iconLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: features
        }),
        updateWhileAnimating: true,
        updateWhileInteracting: true,
	    zIndex: itemZIndex,
        title: itemTitle
    });

    olMap.addLayer(iconLayer);
}

function buildCoordinates(lon, lat) {
    return ol.proj.transform([parseFloat(lat), parseFloat(lon)], 'EPSG:4326', 'EPSG:3857')
}


function getFeatures(point, itemId) {
    let feature = new ol.Feature({geometry: point, id: itemId});
    feature.setStyle(changeScale);
    return [feature];
}

function changeScale(feature, resolution) {
    // This method is called every time someone zooms.
    let scaledResolution = 0.01 / resolution;

    let id = feature.getProperties().id;
    let icon_path = idToIconMapping[id];

    let iconStyle = new ol.style.Icon({
        scale: scaledResolution,
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: icon_path
    });

    return new ol.style.Style({image: iconStyle});
}

function addTranslateInteraction() {
    var itemLayer = getItemLayers(olMap);

    var translate = new ol.interaction.Translate({
        layers: itemLayer
    });

    addCallbackOfTranslate(translate);
    olMap.addInteraction(translate);
}

function getBuildingLayers(map) {
    let buildingLayers = [];
    map.getLayers().getArray().forEach(function getCorrectLayers(layer){
        if (layer.get('title') === buildingTitle)
            buildingLayers.push(layer)
    });
    return buildingLayers;
}

function getItemLayers(map) {
    let itemLayers = [];
    map.getLayers().getArray().forEach(function getCorrectLayers(layer){
        if (layer.get('title') === itemTitle)
            itemLayers.push(layer)
    });
    return itemLayers;
}

function addCallbackOfTranslate(translate) {
    var csrfToken = getCookie('csrftoken');
    var coordinates;
    translate.on('translatestart', function (e) {
        coordinates = e.features.getArray()[0].getGeometry().getCoordinates();
    })
    translate.on('translateend', function (e) {
        if (coordinates[0] != e.features.getArray()[0].getGeometry().getCoordinates()[0] ||
                                        coordinates[1] != e.features.getArray()[0].getGeometry().getCoordinates()[1]) {
            let translatedFeature = e.features.getArray()[0];
            let itemId = translatedFeature.values_.id;
            let location = translatedFeature.getGeometry().getCoordinates();
            let lonLat = ol.proj.transform(location, 'EPSG:3857', 'EPSG:4326');

            var date = new Date();
            var now = prompt("Please enter the date of this change in ISO 8601", date.toISOString());

            if (now != null) {
                $.ajax({
                    url: '/items/edit/' + itemId,
                    type: "POST",
                    data: {'id': itemId, 'lon': lonLat[0], 'lat': lonLat[1], 'date': now},
                    headers: {'X-CSRFToken': csrfToken}
                })
            } else {
                translatedFeature.getGeometry().setCoordinates(coordinates);
            }
         }
    });
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


function removeAllFloorLayers() {
    let buildingLayers = getBuildingLayers(olMap);
    buildingLayers.forEach(function removeLayer(layer) {
        olMap.removeLayer(layer);
    });
}


function removeAllItemLayers() {
    let itemLayers = getItemLayers(olMap);
    itemLayers.forEach(function removeLayer(layer) {
        olMap.removeLayer(layer);
    });
}

function linkToBuildingPage(){
    var map = olMap;
    var select = null;
    var selectSingleClick = new ol.interaction.Select();

    var changeInteraction = function() {
        if (select != null) {
            map.removeInteraction(select);
        }
        select = selectSingleClick;
        map.addInteraction(select);
        select.on('select', function(e) {
            var building = e.selected[0].values_.building;
            var shortbuilding = building.replace(/ *\([^)]*\) */g, "");
            window.location.href = "../buildings/".concat(shortbuilding, '/');
        })
    };

    changeInteraction();
}


function loadBuildingView(building_name) {
    $.ajax({
        url: "/buildings/" + building_name + "/location",
        success: function (data) {
            if (data) {
                olMap.getView().setCenter(ol.proj.fromLonLat([data.lon, data.lat]));
                olMap.getView().setZoom(19);
                }
            }
    })
}