#!/bin/bash

BUILDING=$1
OUTFILE=`date +$BUILDING-%Y%m%d.json`
wget "https://api.mazemap.com/search/equery/?q=$BUILDING&rows=500&start=0&withpois=true&withbuilding=true&withtype=true&withcampus=true&campusid=171" -O $OUTFILE


FLOORIDS=`cat $OUTFILE | jq '.' | grep floorId | sort | uniq`
rm $OUTFILE
FLOORIDS="${FLOORIDS//\"floorId\":/}"
FLOORIDS="${FLOORIDS//\,/}"
mkdir $BUILDING
cd $BUILDING
for x in $FLOORIDS; do
  wget "https://api.mazemap.com/api/pois/?campusid=171&floorid=$x" -O "$x.json"
done
