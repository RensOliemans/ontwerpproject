import os
import json
from .room import RoomModel

buildings_dir = '/home/rens/Documents/studie/11_Ontwerpproject/wappie/static/geojson'


def main():
    buildings = os.listdir(buildings_dir)
    result = dict()
    for building in buildings:
        if building == 'mazemap_dump.sh' or building == 'toGeoJSON.py':
            continue
        floors = get_floors_of_building(buildings_dir + '/' + building)
        result[building] = parse_floors(building, floors)
    return result


def get_floors_of_building(building_dir):
    floors = os.listdir(building_dir)
    floors = [f for f in floors if get_extension(f) == 'geojson']
    return floors


def get_extension(filename):
    return filename.split('.')[-1]


def parse_floors(building_name, floors):
    rooms_of_floor = dict()
    for floor in floors:
        filename = buildings_dir + '/' + building_name + '/' + floor
        rooms_of_floor[get_floornumber(floor)] = list(parse_floor(filename))
    return rooms_of_floor


def get_floornumber(floor):
    return floor.split('.')[0].split('-')[-1]


def parse_floor(filename):
    json_file = json.load(open(filename))
    rooms = json_file['features']
    for room in rooms:
        try:
            yield parse_room(room)
        except ValueError:
            continue


def parse_room(room):
    coordinates = room['geometry']['coordinates'][0]
    properties = room['properties']
    if type(coordinates) != list:
        raise ValueError("coordinates are incorrect")
    if properties['title'] is None:
        raise ValueError("room has no title")
    return RoomModel(building_name=properties['buildingName'], floor_number=properties['floorName'], room_name=properties['title'], poid='', location_data=coordinates)
