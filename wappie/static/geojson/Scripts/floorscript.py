from static.geojson.Scripts import script
from buildings.models import Building, Floor, Room
from django.db import IntegrityError
from django.db import DataError

buildings = script.main()
del buildings['Scripts']
for building in buildings:
    print(building)
    b = Building.objects.get(name=building)
    for floor in buildings[building]:
        f, created = Floor.objects.get_or_create(building=b, floor_number=floor)
        for room in buildings[building][floor]:
            y, x = room.middle.x, room.middle.y
            try:
                Room.objects.get_or_create(floor=f, room_code=room.room_name, middle_of_room_location=[x, y])
            except (IntegrityError, DataError):
                continue
