from shapely.geometry import Polygon


class RoomModel(object):

    """Docstring for Room. """

    def __init__(self, building_name, floor_number, room_name, poid, location_data):
        """TODO: to be defined1.

        :building_name: TODO
        :floor_number: TODO
        :room_name: TODO
        :poid: TODO
        :location_data: TODO

        """
        self.building_name = building_name
        self.floor_number = floor_number
        self.room_name = room_name
        self._poid = poid
        self.middle = self.get_middle_of_room(location_data)

    @staticmethod
    def get_middle_of_room(location_data):
        """TODO: Docstring for get_middle_of_room.

        :location_data: TODO
        :returns: TODO

        """
        polygon = Polygon(location_data)
        return polygon.centroid

    def __repr__(self):
        return f"{self.room_name}, middle: {self.middle}"
