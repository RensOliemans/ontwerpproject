#! usr/bin/env python

###############
# How to use:
# Copy the file to the folder containing the input files.await
# Execute the script as follows: python toGeoJSON.py inputfile.json outputfile.json
###############

from sys import argv
import simplejson as json
from pyproj import Proj, transform

script, in_file, out_file = argv

data = json.load(open(in_file))

inProj = Proj("+init=EPSG:3857")
outProj = Proj("+init=EPSG:4326")


# To change to lat, long template:
# def trans1(c1, c2):
#     x1, y1 = transform(inProj, outProj, c1, c2)
#     return y1
#
# def trans2(c1, c2):
#     x1, y1 = transform(inProj, outProj, c1, c2)
#     return x1


def trans(c1, c2):
    """
    Function that transforms the two given coordinates c1 and c2 from EPSG:3857 to EPSG:4326
    :param c1:
    :param c2:
    :return:
    """
    x1, y1 = transform(inProj, outProj, c1, c2)
    return x1, y1


def coord(dd, type):
    """
    Function that changes the list of coordinates from EPSG 3857 to EPSG 4326
    :param dd:
    :param type:
    :return:
    """
    coords = dd['geometry']['coordinates']
    if type == "Point":
        coords[0], coords[1] = trans(coords[0], coords[1])
    else:
        for j in range(0, len(coords)):
            for i in range(0, len(coords[j])):
                coords[j][i][0], coords[j][i][1] = trans(coords[j][i][0], coords[j][i][1])
                # If we want to change the coordinates from long, lat to lat, long:
                # temp = coords[0][i][0]
                # coords[0][i][0] = trans2(coords[0][i][0], coords[0][i][1])
                # coords[0][i][1] = trans1(temp, coords[0][i][1])
    return coords


geojson = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": d["geometry"]["type"],
                "coordinates": coord(d, d["geometry"]["type"])
            },
            "properties": {
                "buildingName": d["buildingName"],
                "floorName": d["floorName"],
                "title": d["title"],
            },
        } for d in data["pois"]]
}

output = open(out_file, 'w')
json.dump(geojson, output)
