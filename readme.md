# Ontwerpproject

## Wappie - Wifi Access Point Platform for Improved Editing



### Setup instructions
In `wappie/wappie` - in the main Django app where the `settings.py` file is
located, a `.env` file needs to be created. In there the following variables
need to be initialised:

* SECRET_KEY
* POSTGRESQL_DATABASENAME
* POSTGRESQL_USERNAME
* POSTGRESQL_PASSWORD

In addition, such a PostgreSQL database needs to be created with appropriate
permissions.
